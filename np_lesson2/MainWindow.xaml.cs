﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace np_lesson2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //Стандартные ip адреса
            cbIpServer.Items.Add("0.0.0.0");
            cbIpServer.Items.Add("127.0.0.1");
            //Адреса локальной машины
            foreach (var item in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
            {
                cbIpServer.Items.Add(item.ToString());
            }
            cbIpServer.SelectedIndex = 0;
            //Dns.GetHostEntry(Dns.GetHostName());
            //Флаг - запущен сервер или нет
            btnStart.Tag = false;
            isStopServer = false;
        }
        Thread srvThread;
        TcpListener srvSocket;
        bool isStopServer;


        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)btnStart.Tag == false)
            {//Запускаем сервер   

                srvSocket = new TcpListener(IPAddress.Parse(cbIpServer.SelectedItem.ToString()), int.Parse(tbPort.Text)); //Указываем ip адресс и порт
                //Начало прослушивания сети по ip:port
                srvSocket.Start(100);

                //Должна быть команда Accept

                srvThread = new Thread(ServerThreadRoutine);

                //srvThread.IsBackground = true;
                srvThread.Start(srvSocket);

                btnStart.Content = "Stop";
                btnStart.Tag = true;
            }
            else
            {//Останавливаем сервер
                btnStart.Content = "Start";
                btnStart.Tag = false;
                isStopServer = true;
            }
        }
        void ServerThreadRoutine(object obj)
        {
            TcpListener srvSocket = obj as TcpListener;
            //Асинхронный вариант
            while (true)
            {

                IAsyncResult ia = srvSocket.BeginAcceptTcpClient(ClientThreadRoutine, srvSocket);
                //Ожидание завершения асинхроного вызова
                //AutoResetEvent myEvent(false);
                //MyEvent.WaitOne();
                while (ia.AsyncWaitHandle.WaitOne(200) == false)
                {
                    //WaitOne Завершился по  тайм ауту
                    if (isStopServer == true)
                    {
                        //Завершение приема удаленных клиентов
                        //srvSocket.EndAcceptSocket(ia); // ???
                        //CancellationToken ???
                        return;
                    }

                }

            }

            #region Синхронный вариант
            ////Синхронный вариант
            //while (true)
            //{
            //    //не асинхронный блокриующии вызов Accept()
            //    //ожидание подключения удаленного клиента
            //    TcpClient client = srvSocket.AcceptTcpClient();
            //    //запуск клиентского потока - работа клиента в отдельном потоке
            //    ThreadPool.QueueUserWorkItem(ClientThreadRoutine, client);
            //} 
            #endregion
        }

        void ClientThreadRoutine(IAsyncResult ia)
        {
            TcpListener srvSocket = ia.AsyncState as TcpListener;
            TcpClient client = srvSocket.EndAcceptTcpClient(ia);
            ThreadPool.QueueUserWorkItem(ClientThreadRoutine2, client);
        }

        delegate void DelegateAppendTextBox(TextBox textBox, string str);
        void AppendTextBox(TextBox textBox, string str)
        {
            textBox.AppendText(str + "\n");
        }

        void ClientThreadRoutine2(object obj)
        {// Поток работы с удаленным клиентом
            TcpClient client = obj as TcpClient;
            //вывод информации в журнал о соединении
            Dispatcher.Invoke(() =>
            {
                tbJournal.AppendText("Успешное содинение клиента\n");
            });
            string s = "IP:port клиента" + client.Client.RemoteEndPoint.ToString();
            Dispatcher.Invoke(new DelegateAppendTextBox(AppendTextBox), tbJournal, s);
            // дальше по протоколу
            byte[] buf = new byte[4 * 1024];
            //1 - ждем от клиента его имени
            int recSize = client.Client.Receive(buf);
            
            //2 - ответ сервера
            string clientName = Encoding.UTF8.GetString(buf, 0, recSize);
            client.Client.Send(Encoding.UTF8.GetBytes("Hello " + clientName + "!"));
            recSize = 1;
            while (recSize>0)
            {
                recSize = client.Client.Receive(buf);
                Dispatcher.Invoke(new DelegateAppendTextBox(AppendTextBox), tbJournal, Encoding.UTF8.GetString(buf, 0, recSize));
                //foreach (var item in collection)
                //{

                //}
                client.Client.Send(buf, recSize, SocketFlags.None);
            }
        }
    }
}
